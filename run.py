from email import message
from multiprocessing.dummy import active_children
from threading import activeCount
from flask import Flask, render_template, url_for, request, redirect, flash

app = Flask(__name__)

# region ===================================== DATABASE =====================================
import mysql.connector
DB_NAME = 'website_sekolah'
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASSWORD = ''

def dbConn(user=DB_USER, password=DB_PASSWORD, host=DB_HOST, database=DB_NAME):
    db = mysql.connector.connect (
		host=host,
		user=user,
		password=password,
		database=database
	)
    # cek koneksi
    # if db.is_connected():
    #     print("Berhasil terhubung ke database")
    return db

def selall(query, values, conn):
	mycursor = conn.cursor(buffered=True)
	mycursor.execute(query, values)
	row_headers = [x[0] for x in mycursor.description]
	myresults = mycursor.fetchall()
	json_data = []
	if myresults != None:
		for myresult in myresults:
			result = [(x.decode() if type(x)==bytearray else x) for x in myresult] # decode bytearray
			json_data.append(dict(zip(row_headers, result)))
		return json_data
	else:
		return None

# get single row
def selrow(query, values, conn):
	mycursor = conn.cursor(buffered=True)
	mycursor.execute(query, values)
	row_headers = [x[0] for x in mycursor.description]
	myresult = mycursor.fetchone()
	if myresult != None:
		result = [(x.decode() if type(x)==bytearray else x) for x in myresult] # decode bytearray
		return dict(zip(row_headers, result))
	else:
		return None

# insert dan update
def executeQuery(query, val, conn):
	mycursor = conn.cursor()
	mycursor.execute(query, val)
	conn.commit()

# endregion ===================================== DATABASE =====================================

@app.route('/')
@app.route('/home/')
def home():
    return render_template("beranda.html", title="Beranda | SMA Negeri 188 Jakarta", active_url="/")

@app.route('/berita')
def berita():
    return render_template("berita.html", title="Berita | SMA Negeri 188 Jakarta", active_url="/berita")

@app.route('/about')
def about():
    return render_template("about.html", title="About | SMA Negeri 188 Jakarta", active_url="/about")

@app.route('/dataSiswa', methods=['GET'])
def dataSiswa():
	data = getSiswa()
    
	# Index data
	i = 1
	for item in data:
		item['index'] = i
		i += 1	
	# End Index data
	
	try:
		message = request.args['message']
		return render_template("crudSiswa/dataSiswa.html", title="Data Siswa | SMA Negeri 188 Jakarta", active_url="/dataSiswa", dataSiswa=data, message=message)
	except Exception:
		return render_template("crudSiswa/dataSiswa.html", title="Data Siswa | SMA Negeri 188 Jakarta", active_url="/dataSiswa", dataSiswa=data)

@app.route('/tambahSiswa')
def tambahSiswa():
    return render_template("crudSiswa/tambahSiswa.html", title="Data Siswa | SMA Negeri 188 Jakarta", active_url="/dataSiswa")

@app.route('/editSiswa/<id>', methods=['GET'])
def editSiswa(id):
	data = getRow(id)
	return render_template("crudSiswa/editSiswa.html", title="Edit Data Siswa | SMA Negeri 188 Jakarta", active_url="/dataSiswa", dataSiswa=data)

# region ===================================== ROUTING CRUD =====================================
def getSiswa():
	query = "select * from siswa"
	values = ()
	data = selall(query, values, dbConn())
	return data

def getRow(id):
	query = "select * from siswa where id=%s"
	values = (id, )
	data = selrow(query, values, dbConn())
	return data

@app.route('/insertrow', methods=['POST'])
def insertrow():
	dataUser = request.form.to_dict()
	query = "INSERT INTO siswa (nama, nisn, no_telp, jenis_kelamin, alamat) values (%s, %s, %s, %s, %s)"
	values = (dataUser['nama'], dataUser['nisn'], dataUser['no_telp'], dataUser['jenis_kelamin'], dataUser['alamat'])
	executeQuery(query, values, dbConn())
	return redirect(url_for("dataSiswa", message="Berhasil Menambahkan Data Siswa"))

@app.route('/updaterow', methods=['PUT', 'GET'])
def updaterow():
	dataUser = request.args.to_dict()
	query = "update siswa set nama=%s, nisn=%s, no_telp=%s, jenis_kelamin=%s, alamat=%s WHERE id=%s"
	values = (dataUser['nama'], dataUser['nisn'], dataUser['no_telp'], dataUser['jenis_kelamin'], dataUser['alamat'], dataUser['id'])
	executeQuery(query, values, dbConn())
	return redirect(url_for("dataSiswa", message="Berhasil Mengupdate Data Siswa"))

@app.route('/deleterow/<id>', methods=['PUT', 'GET'])
def deleterow(id):
	query = "DELETE FROM siswa WHERE id=%s"
	values = (id, )
	executeQuery(query, values, dbConn())
	return redirect(url_for("dataSiswa", message="Berhasil Menghapus Data Siswa"))

# endregion ===================================== ROUTING CRUD =====================================

if __name__ == '__main__':
    app.run(debug=True, port=12345)